@author=Bhagyasree
/* A program to demonstrate dynamic loading of classes
(loading of classes at runtime)
and then introspection using reflection*/


import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
class abc 
{ 
int a;
abc()
{
a=10;
}
public void alfa(int n)
{
 System.out.println("The public method of abc is accessed and n is " + n); 
}
}
class def
{
int d;
def()
{
d=30;
}
public void beta(int m)
{
 System.out.println("The public method of def is accessed and m is " + m); 
}
}
class DynamicLoading
{
	public static void main(String[] args)throws Exception
	{
		try
		{
			Class c1=Class.forName(args[0]);
			Object ob1=(Object)c1.newInstance();
        System.out.println("The public methods of class c1 are : "); 
		Method[] methd = c1.getMethods();  
        for (Method md:methd) 
            System.out.println(md.getName());
        Method alfacall = c1.getDeclaredMethod("alfa", int.class);
           alfacall.invoke(ob1,19); 
        }
    		catch(ClassNotFoundException c)
		{
            System.out.println("Class Not Found Exception");

          }
          try
          {
          	Class c2=Class.forName(args[1]);
          	Object ob2=(Object)c2.newInstance();
		  System.out.println("The public methods of class c2 are : "); 
		Method[] methods = c2.getMethods();  
        for (Method meth:methods) 
            System.out.println(meth.getName());
                Method betacall = c2.getDeclaredMethod("beta", int.class);
        betacall.invoke(ob2, 19); 
          }
          catch(ClassNotFoundException p)
		{
            System.out.println("Class Not Found Exception");

          }
	}
}